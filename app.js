const express = require("express")
const mongoose = require("mongoose")
const dotenv = require("dotenv")
const taskRoute = require("./Routes/taskRoutes.js")

dotenv.config()

//server setup
const app = express()
const port = 3003

app.use(express.json())
app.use(express.urlencoded({extended: true}))

//mongodb connection
mongoose.connect(`mongodb+srv://bks0381:${process.env.MONGODB_PASSWORD}@cluster0.0atlgec.mongodb.net/S36-todo?retryWrites=true&w=majority`, {
	useNewUrlParser: true,
	useUnifiedTopology: true
})

let db = mongoose.connection

db.on("error", () => console.error(`Connection error.`))

db.on("open", () => console.log(`We are connected to MongoDB!`))

app.use("/tasks", taskRoute)

//server listening
app.listen(port, () => {
	console.log(`Server is running on localhost ${port}`)
})