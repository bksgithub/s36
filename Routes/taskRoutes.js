const express = require("express")
const router = express.Router()
const TaskController = require("../Controllers/TaskController")

//create single task
router.post("/create", (request, response) => {
	TaskController.createTask(request.body).then((result) => {
		response.send(result)
	})
})

//get all tasks
router.get("/", (request, response) => {
	TaskController.getAllTasks().then((result) => {
		response.send(result)
	})
})

//update a task
router.patch("/:id/update", (request, response) => {
	TaskController.updateTask(request.params.id, request.body).then((result) => {
		response.send(result)
	})
})

//delete task
/*
1. set up a route for deleting task
2. set up the controller for deleting task
3. build the controller function responsible for deleting task
4. send the result of that controller function as a response

endpoint /:id/delete */

router.delete("/:id/delete", (request, response) => {
	TaskController.deleteTask(request.params.id).then((result) => {

		response.send(`Task ID ${request.params.id} has been deleted.`)
	})
})

/*
1. Create a route for getting a specific task.
*/
/*
4. Process a GET request at the /tasks/:id route using postman to get a
specific task*/
router.get("/:id", (request, response) => {
	TaskController.getSpecificTask(request.params.id).then((result)  => {
		response.send(result)
	})
})

/*5. Create a route for changing the status of a task to complete.
*/
/*
8. Process a PUT request at the /tasks/:id/complete route using
postman to update a task*/
router.put("/:id/complete", (request, response) => {
	TaskController.changeStatusToComplete(request.params.id, request.body).then((result) => {
		response.send(result)
	})
})

module.exports = router