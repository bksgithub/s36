const Task = require("../Models/task.js")

module.exports.createTask = (data) => {
	let newTask = new Task({
		name: data.name
	})

	return newTask.save().then((savedTask, error) => {
		if (error) {
			console.log(error)
			return error
		}

		return savedTask
	})
}

module.exports.getAllTasks = () => {
	return Task.find({}).then((result) => {
		return result
	})
}

module.exports.updateTask = (task_id, new_data) => {
	return Task.findById(task_id).then((result, error) => {
		if (error) {
			console.log(error)
			return error
		}

		result.name = new_data.name

		return result.save().then((updatedTask, error) => {

			if (error) {
				console.log(error)
				return error
			}

			return updatedTask
		})
	})
}


module.exports.deleteTask = (task_id) => { 
	return Task.findByIdAndDelete(task_id).then((removedTask, error) => {
		if (error) {
			console.log(error)
			return error
		}

		return removedTask
	})
}

/*
2. Create a controller function for retrieving a specific task.*/
/*
3. Return the result back to the client/Postman.*/
module.exports.getSpecificTask = (task_id) => {
	return Task.findById(task_id).then((result, error) => {
		if (error) {
			console.log(error)
			return error
		}

		return result
	})
}


/*
6. Create a controller function for changing the status of a task to
complete*/
module.exports.changeStatusToComplete = (task_id, new_status) => {
	return Task.findById(task_id).then((result, error) => {
		if (error) {
			console.log(error)
			return error
		}

		result.status = new_status.status

		return result.save().then((updatedStatus, error) => {

			if (error) {
				console.log(error)
				return error
			}
/*
7. Return the result back to the client/Postman.
*/
			return updatedStatus
		})
	})
}